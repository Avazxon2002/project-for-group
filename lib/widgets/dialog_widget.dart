import 'package:flutter/material.dart';

class DialogWidget extends StatefulWidget {
  const DialogWidget({Key? key}) : super(key: key);

  @override
  _DialogWidgetState createState() => _DialogWidgetState();
}

class _DialogWidgetState extends State<DialogWidget> {
  final _formKey = GlobalKey<FormState>();
  String name = '';
  final TextEditingController _textEditingController = TextEditingController();

  _submit() {}

  @override
  Widget build(BuildContext context) => AlertDialog(
      title: const Text('Add Faculty'),
      actions: [
        TextButton(
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              _submit();
            }
          },
          child: const Text('Save'),
        )
      ],
      content: Form(
          key: _formKey,
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            TextFormField(
              controller: _textEditingController,
              validator: (value) {
                return value!.isNotEmpty ? null : "Enter faculty";
              },
              decoration: const InputDecoration(
                labelText: 'Faculty name',
              ),
            ),
          ])));
}

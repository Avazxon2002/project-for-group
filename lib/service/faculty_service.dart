import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:project_for_portfolio/model/faculty_model.dart';
import 'package:http/http.dart' as http;

class FacultyService{
  static const String facultyUrl = "https://jsonplaceholder.typicode.com/posts";

  static List<FacultyModel> parseFaculty(String responseBody){
    var list = json.decode(responseBody) as List<dynamic>;
    List<FacultyModel> faculties = list.map((model) => FacultyModel.fromJson(model)).toList();
    return faculties;
  }

  static Future<List<FacultyModel>> fetchFaculties() async{
    final response = await http.get(Uri.parse(facultyUrl));
    if (response.statusCode == 200) {
      return compute(parseFaculty, response.body);
    } else{
      throw Exception('Can`t get faculties');
    }
  }
}
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:project_for_portfolio/pages/faculty_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => FacultyPage();
}
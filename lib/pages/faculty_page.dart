import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:project_for_portfolio/model/faculty_model.dart';
import 'package:project_for_portfolio/pages/students_page.dart';
import 'package:project_for_portfolio/widgets/dialog_widget.dart';
import '../home_page.dart';
import 'courses_page.dart';
import 'groups_page.dart';

class FacultyPage extends State<HomePage> {
  List<FacultyModel>? faculty;

  void facultiesTap() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => const HomePage()));
  }

  void groupsTap() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => const GroupsPage()));
  }

  void studentsTap() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => const StudentsPage()));
  }

  void coursesTap() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => const CoursesPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Faculty Page"),
      ),
      body: FutureBuilder(builder: (context, snapshot) {
        if (snapshot.hasData) {
          return
              // Lottie
              Center(
            child: Lottie.asset('assets/lottie/85646-loading-dots-blue.json',
                height: 150),
          );
        } else {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView.builder(
                itemCount: 15,
                itemBuilder: (context, index) {
                  return SizedBox(
                    height: 70,
                    child: Card(
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                child: const Text("hi"),
                                onTap: () => groupsTap(),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                }),
          );
        }
      }),

      //FloatActionButton
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
              context: context,
              builder: (context) => const DialogWidget(),
              barrierDismissible: false);
        },
        child: const Icon(Icons.add),
      ),

      // NavigationDrawer
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const SizedBox(
                height: 85,
                child: DrawerHeader(
                    decoration: BoxDecoration(color: Colors.blue),
                    child: Text(
                      "Faculty",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ))),
            ListTile(
              title: const Text('Faculty'),
              onTap: () => facultiesTap(),
            ),
            ListTile(
              title: const Text('Groups'),
              onTap: () => groupsTap(),
            ),
            ListTile(
              title: const Text('Students'),
              onTap: () => studentsTap(),
            ),
            ListTile(
              title: const Text('Courses'),
              onTap: () => coursesTap(),
            ),
          ],
        ),
      ),
    );
  }
}

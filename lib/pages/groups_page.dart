import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:project_for_portfolio/pages/students_page.dart';

class GroupsPage extends StatefulWidget {
  const GroupsPage({Key? key}) : super(key: key);

  @override
  State<GroupsPage> createState() => _GroupsPage();
}

class _GroupsPage extends State<GroupsPage> {

  void studentsTap() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => const StudentsPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Groups Page'),
      ),
      body: FutureBuilder(builder: (context, snapshot) {
        if (snapshot.hasData) {
          return
              // Lottie
              Center(
            child: Lottie.asset('assets/lottie/85646-loading-dots-blue.json',
                height: 150),
          );
        } else {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView.builder(
                itemCount: 15,
                itemBuilder: (context, index) {
                  return SizedBox(
                    height: 70,
                    child: Card(
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                child: const Text("groups"),
                                onTap: () => studentsTap(),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                }),
          );
        }
      }),

      //FloatActionButton
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.add),
      ),
    );
  }
}

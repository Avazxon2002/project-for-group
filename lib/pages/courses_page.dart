import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class CoursesPage extends StatefulWidget {
  const CoursesPage({Key? key}) : super(key: key);

  @override
  State<CoursesPage> createState() => _CoursesPage();
}

class _CoursesPage extends State<CoursesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Courses Page'),
      ),
      body: FutureBuilder(builder: (context, snapshot) {
        if (snapshot.hasData) {
          return
              // Lottie
              Center(
            child: Lottie.asset('assets/lottie/85646-loading-dots-blue.json',
                height: 150),
          );
        } else {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView.builder(
                itemCount: 15,
                itemBuilder: (context, index) {
                  return SizedBox(
                    height: 70,
                    child: Card(
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [Text("courses")],
                          )
                        ],
                      ),
                    ),
                  );
                }),
          );
        }
      }),

      //FloatActionButton
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.add),
      ),
    );
  }
}
